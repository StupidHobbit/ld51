// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/EngineTypes.h"
#include "God.generated.h"


UENUM()
enum GodStatus
{
	Peaceful UMETA(DisplayName = "Peaceful"),
	Dissatisfied UMETA(DisplayName = "Dissatisfied"),
	Angry UMETA(DisplayName = "Angry"),
	Punishing UMETA(DisplayName = "Punishing"),
	TheEnd UMETA(DisplayName = "TheEnd")
};

UCLASS()
class LD51_API AGod : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AGod();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Status)
	TEnumAsByte<GodStatus> status;

	FTimerHandle StatusTimerHandle;

	void DowngradeStatus();

	UFUNCTION(BlueprintCallable)
	void StartTimer();

	UFUNCTION(BlueprintCallable)
	void StopTimer();

	UFUNCTION(BlueprintCallable)
	void Satisfy();

	UFUNCTION(BlueprintImplementableEvent)
	void OnDowngrade();

	UFUNCTION(BlueprintImplementableEvent)
	void OnSatisfy();

	UFUNCTION(BlueprintCallable)
	bool IsEnd();

	void SetTimer();


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
