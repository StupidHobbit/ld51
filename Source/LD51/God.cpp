// Fill out your copyright notice in the Description page of Project Settings.


#include "God.h"
#include "TimerManager.h"

// Sets default values
AGod::AGod()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	this->status = GodStatus::Peaceful;
}

// Called when the game starts or when spawned
void AGod::BeginPlay()
{
	Super::BeginPlay();
	this->StartTimer();
}

void AGod::DowngradeStatus() {
	if (this->status == GodStatus::Peaceful) {
		this->status = GodStatus::Dissatisfied;
	} else if (this->status == GodStatus::Dissatisfied) {
		this->status = GodStatus::Angry;
	} else if (this->status == GodStatus::Angry) {
		this->status = GodStatus::Punishing;
	} else if (this->status == GodStatus::Punishing) {
		this->status = GodStatus::TheEnd;
	}
	this->OnDowngrade();
}

void AGod::StartTimer() {
	this->SetTimer();
}

void AGod::StopTimer() {
	GetWorldTimerManager().ClearTimer(StatusTimerHandle);
}

void AGod::Satisfy() {
	this->status = GodStatus::Peaceful;
	this->SetTimer();
	this->OnSatisfy();
}

bool AGod::IsEnd()
{
	return this->status == GodStatus::TheEnd;
}

void AGod::SetTimer() {
	GetWorldTimerManager().SetTimer(StatusTimerHandle, this, &AGod::DowngradeStatus, 10.0f, true, 10.0f);
}



// Called every frame
void AGod::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

