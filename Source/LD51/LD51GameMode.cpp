// Copyright Epic Games, Inc. All Rights Reserved.

#include "LD51GameMode.h"
#include "LD51Character.h"
#include "UObject/ConstructorHelpers.h"

ALD51GameMode::ALD51GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	this->LambsSacrificed = 0;
}

void ALD51GameMode::LambSacrificed() {
	this->LambsSacrificed += 1;
}

bool ALD51GameMode::GodSatisfied()
{
	return this->LambsSacrificed >= this->LambsToWin;
}
